#include "assignment.h"

enum {ROW = 0, COL = 1, BLK = 2};

Constraint &Assignment::row(Value index)
{
	return asst[ROW][index];
}

Constraint const&Assignment::row(Value index) const
{
	return asst[ROW][index];
}

Constraint const&Assignment::column(Value index) const
{
	return asst[COL][index];
}

Constraint &Assignment::column(Value index)
{
	return asst[COL][index];
}

Constraint &Assignment::block(Value index)
{
	return asst[BLK][index];
}

Constraint const&Assignment::block(Value index) const
{
	return asst[BLK][index];
}
