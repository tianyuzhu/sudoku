#include "search.h"
#include <ostream>

using std::ostream;

Result::Result() :
searchedNodes(0),
solved(false)
{}

Result &Result::operator +=(Result result)
{
	searchedNodes += result.searchedNodes;
	solved = solved || result.solved;
	return *this;
}

ostream &operator <<(ostream &out, const Result &result)
{
	if (!result.solved) out << "NOT ";
	return out << "Solved in " << result.searchedNodes << " searches";
}
