#include "states.h"
#include "puzzle.h"
#include "assignment.h"

/// Declarations ///

static bool updateConstraints(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Value value);

static void removeConstraints(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Value value);

static bool forwardCheck(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Puzzle& puzzle);

/// Assign ///

template<bool FC>
Assign<FC>::Assign(Assign &&assign) :
valid(assign.valid),
entry(assign.entry),
puzzle(assign.puzzle),
asst(assign.asst)
{
	assign.entry = nullptr;
}

template Assign<false>::Assign(Assign &&);
template Assign<true>::Assign(Assign &&);

template<>
Assign<false>::Assign(
		Value &entry, Value value,
		Puzzle &puzzle, Assignment &asst) :
entry(nullptr),
puzzle(puzzle),
asst(asst)
{
	auto coords = puzzle.coordsOf(entry);

	// Check to see if the assignment is valid
	valid = updateConstraints(asst, coords, value);

	if (!valid) return;
	this->entry = &entry;
	entry = value;
}

template<>
Assign<true>::Assign(
		Value &entry, Value value,
		Puzzle &puzzle, Assignment &asst) :
entry(nullptr),
puzzle(puzzle),
asst(asst)
{
	auto coords = puzzle.coordsOf(entry);

	// Check to see if the assignment is valid
	valid = updateConstraints(asst, coords, value);

	if (!valid) return;
	this->entry = &entry;
	entry = value;

	// Do forward checking
	valid = forwardCheck(asst, coords, puzzle);
}

template<bool FC>
void Assign<FC>::cancelUndo()
{
	entry = nullptr;
}

template void Assign<false>::cancelUndo();
template void Assign<true>::cancelUndo();

template<bool FC>
bool Assign<FC>::isValid()
{
	return valid;
}

template bool Assign<false>::isValid();
template bool Assign<true>::isValid();

template<bool FC>
Assign<FC>::~Assign()
{
	if (!entry) return;
	Value value = *entry;
	*entry = 0;
	removeConstraints(
		asst,
		puzzle.coordsOf(*entry),
		value);
}

template Assign<false>::~Assign();
template Assign<true>::~Assign();

template<bool FC>
Assign<FC> Assign<FC>::assign(Value &entry, Value value)
{
	return Assign(entry, value, puzzle, asst);
}

template Assign<false> Assign<false>::assign(Value &, Value);
template Assign<true> Assign<true>::assign(Value &, Value);

/// Start ///

template<bool FC>
Start<FC>::Start(Puzzle &puzzle, Assignment &asst) :
puzzle(puzzle),
asst(asst),
valid(true)
{
	for (int i=0; i<81 && valid; ++i) {
		Value &entry = puzzle[i];
		valid = updateConstraints(
			asst,
			puzzle.coordsOf(entry),
			entry);
	}
}

template Start<false>::Start(Puzzle&, Assignment&);
template Start<true>::Start(Puzzle&, Assignment&);

template<bool FC>
void Start<FC>::cancelUndo()
{}

template void Start<false>::cancelUndo();
template void Start<true>::cancelUndo();

template<bool FC>
bool Start<FC>::isValid()
{
	return valid;
}

template bool Start<false>::isValid();
template bool Start<true>::isValid();

template<bool FC>
Assign<FC> Start<FC>::assign(Value& entry, Value value)
{
	return Assign<FC>(entry, value, puzzle, asst);
}

template Assign<false> Start<false>::assign(Value&, Value);
template Assign<true> Start<true>::assign(Value&, Value);

/// Constraint Checking ///

enum {Row = 0, Col = 1, Blk = 2};

static bool updateConstraints(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Value value)
{
	if (value == 0) return true;

	Constraint
	&row = asst.row(coords.row),
	&col = asst.column(coords.column),
	&blk = asst.block(coords.block);

	if (!row.add(value))
		return false;

	if (!col.add(value)) {
		row.remove(value);
		return false;
	}

	if (!blk.add(value)) {
		col.remove(value);
		row.remove(value);
		return false;
	}

	return true;
}

static void removeConstraints(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Value value)
{
	if (value == 0) return;

	Constraint
	&row = asst.row(coords.row),
	&col = asst.column(coords.column),
	&blk = asst.block(coords.block);

	row.remove(value);
	col.remove(value);
	blk.remove(value);
}

/// Forward Checking ///

template<typename Region>
static bool checkRegion(
	Assignment &asst,
	Puzzle &puzzle, Region region)
{
	for (auto i : Value::Indicies) {
		Value &entry = region[i];
		if (entry != 0) continue;
		auto coords = puzzle.coordsOf(entry);
		auto availableValues = Constraint::All ^ (
			asst.row(coords.row) |
			asst.column(coords.column) |
			asst.block(coords.block));
		if (availableValues == Constraint::None)
			return false;
	}
	return true;
}

static bool forwardCheck(
	Assignment &asst,
	Puzzle::Coordinates coords,
	Puzzle &puzzle)
{
	return
	checkRegion(asst, puzzle, puzzle.row(coords.row)) &&
	checkRegion(asst, puzzle, puzzle.column(coords.column)) &&
	checkRegion(asst, puzzle, puzzle.block(coords.block));
}
