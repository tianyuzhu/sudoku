#include "search.h"
#include "puzzle.h"
#include "states.h"
#include "assignment.h"
#include "transition.h"
#include <cstdint>

template<typename State, typename Transition>
Result searchNode(State state, Transition tr)
{
	Result result;

	// Verify the validity of the current puzzle state.
	if (!state.isValid()) return result;

	// Find the next entry that we should search on.
	Value *entry = tr.nextEntry();

	if (!entry) { // Stop if there are no more.
		state.cancelUndo();
		result.solved = true;
		return result;
	}

	// Start searching from this entry.
	++result.searchedNodes;

	// Try assigning values to the entry
	for (Value value : tr.valuesOf(entry)) {
		result += searchNode(state.assign(*entry, value), tr);
		if (result.solved) {
			state.cancelUndo();
			return result;
		}
	}

	return result;
}

template<bool FC, bool H>
Result solve(Puzzle &puzzle)
{
	Assignment asst;
	return searchNode(
		Start<FC>(puzzle, asst),
		Transition<H>(puzzle, asst));
}

template Result solve<false, false>(Puzzle&);
template Result solve<true, false>(Puzzle&);
template Result solve<true, true>(Puzzle&);
