#include "constraint.h"
#include "assignment.h"
#include "puzzle.h"
#include <utility>
#include <algorithm>

#include "transition.h"
using namespace std;

/// Plain Transitions ///

template
Transition<false>::Transition(
	Puzzle &puzzle,
	Assignment const&asst) :
puzzle(puzzle),
index(0)
{}

template
Transition<false>::Transition(Transition const&tr) :
puzzle(tr.puzzle),
index(tr.index + 1) // Increment index
{}

template
Value *Transition<false>::nextEntry()
{
	while (index < 81 && puzzle[index]) ++index;
	return (index < 81)? &puzzle[index] : nullptr;
}

template
const Value (&Transition<false>::valuesOf(Value *entry))[9]
{
	return Value::Values;
}

/// Transitions with Heuristics ///

template
Transition<true>::Transition(
	Puzzle &puzzle,
	Assignment const&asst) :
puzzle(puzzle),
asst(asst)
{}

static vector<Value *> mostConstrainedEntry(
	Puzzle &puzzle, Assignment const&asst);

static Value *mostConstrainingEntry(
	vector<Value *> entries, Puzzle &puzzle);

template
Value *Transition<true>::nextEntry()
{
	vector<Value *> entries = mostConstrainedEntry(puzzle, asst);
	switch (entries.size()) {
	case 0: return nullptr;
	case 1:	return entries[0];
	}
	return mostConstrainingEntry(move(entries), puzzle);
}

static vector<Value> leastConstrainingValues(
	Value *entry, Puzzle& puzzle, Assignment const&asst);

template
vector<Value> Transition<true>::valuesOf(Value *entry)
{
	return leastConstrainingValues(entry, puzzle, asst);
}

/// Heuristics ///

static vector<Value *> mostConstrainedEntry(
	Puzzle &puzzle, Assignment const&asst)
{
	vector<Value *> entries;
	entries.reserve(81);
	int min = 9;

	for (int i = 0; i < 81; ++i) {
		Value &candidate = puzzle[i];
		if (candidate != 0) continue;

		auto coords = puzzle.coordsOf(candidate);
		auto optionSet = Constraint::All ^ (
			asst.row(coords.row) |
			asst.column(coords.column) |
			asst.block(coords.block));

		int options = optionSet.bitCount();
		if (options == min) {
			entries.push_back(&candidate);

		} else if (options < min) {
			min = options;
			entries.clear();
			entries.push_back(&candidate);
		}
	}

	return entries;
}

static Value *mostConstrainingEntry(
	vector<Value *> candidates, Puzzle &puzzle)
{
	Value *entry;
	int max = 0;

	for (Value *candidate : candidates) {
		int constrains = 0;

		auto coords = puzzle.coordsOf(*candidate);
		auto row = puzzle.row(coords.row);
		auto col = puzzle.column(coords.column);
		auto blk = puzzle.block(coords.block);

		int blk_row = coords.block / 3;
		int blk_col = coords.block % 3;

		for (int i = 0; i < 3; ++i) {
			int s = 3*i;
			constrains += !blk[s+0];
			constrains += !blk[s+1];
			constrains += !blk[s+2];
			if (i != blk_col) {
				constrains += !row[s+0];
				constrains += !row[s+1];
				constrains += !row[s+2];
			}
			if (i != blk_row) {
				constrains += !col[s+0];
				constrains += !col[s+1];
				constrains += !col[s+2];
			}
		}

		if (constrains > max) {
			max = constrains;
			entry = candidate;
		}
	}

	return entry;
}

// Measure the constrains on an entry imposed by a value.
static int constrainsOn(
	Value &entry, Value value,
	Puzzle&puzzle, Assignment const&asst)
{
	if (&entry != 0) return 0;
	auto coords = puzzle.coordsOf(entry);
	auto options = Constraint::All ^ (
		asst.row(coords.row) |
		asst.column(coords.column) |
		asst.block(coords.block));
	return options.remove(value)? 1 : 0;
}

// Measure the constrains of a value for an entry
// on then entry's surrounding entries.
static int constrainsOf(
	Value value, Puzzle::Coordinates coords,
	Puzzle &puzzle, Assignment const&asst)
{
	int constrains = 0;

	auto row = puzzle.row(coords.row);
	auto col = puzzle.column(coords.column);
	auto blk = puzzle.block(coords.block);

	int blk_row = coords.block / 3;
	int blk_col = coords.block % 3;

	for (int i = 0; i < 3; ++i) {
		int s = 3*i;
		constrains += constrainsOn(blk[s+0], value, puzzle, asst);
		constrains += constrainsOn(blk[s+1], value, puzzle, asst);
		constrains += constrainsOn(blk[s+2], value, puzzle, asst);
		if (i != blk_col) {
			constrains += constrainsOn(row[s+0], value, puzzle, asst);
			constrains += constrainsOn(row[s+1], value, puzzle, asst);
			constrains += constrainsOn(row[s+2], value, puzzle, asst);
		}
		if (i != blk_row) {
			constrains += constrainsOn(col[s+0], value, puzzle, asst);
			constrains += constrainsOn(col[s+1], value, puzzle, asst);
			constrains += constrainsOn(col[s+2], value, puzzle, asst);
		}
	}
	return constrains;
}

static vector<Value> leastConstrainingValues(
	Value *entry, Puzzle& puzzle, Assignment const&asst)
{
	struct lcv
	{
		lcv(int c, Value v) :
		constrains(c),
		value(v)
		{}

		int constrains;
		Value value;

		bool operator <(lcv const&v) const
		{
			return constrains < v.constrains;
		}
	};

	vector<lcv> lcvs;

	// Get the options
	auto coords = puzzle.coordsOf(*entry);
	auto options = Constraint::All ^ (
		asst.row(coords.row) |
		asst.column(coords.column) |
		asst.block(coords.block));

	// Find all legal values
	for (auto value : Value::Values) {
		if (!options.remove(value)) continue;
		int constrains = constrainsOf(value, coords, puzzle, asst);
		lcvs.push_back(lcv(constrains, value));
	}

	// Sort according to constrains from low to high.
	sort(lcvs.begin(), lcvs.end());

	vector<Value> values;
	for (auto &v : lcvs) {
		values.push_back(v.value);
	}
	return values;
}
