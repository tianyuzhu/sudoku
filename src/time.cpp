#include "puzzle.h"
#include "search.h"
#include "sky/timer.h"
#include <iostream>
#include <cassert>
#include <cstdlib>

using namespace std;
using namespace sky;

struct TimingResult {
	long millis;
	unsigned long nodes;

	TimingResult &operator +=(TimingResult const&r)
	{
		millis += r.millis;
		if (!nodes) nodes = r.nodes;
		assert(nodes == r.nodes);
		return *this;
	}
};

template<bool ForwardChecking, bool Heuristics>
static TimingResult time_solve(Puzzle puzzle)
{
	timer t(nullptr);
	Result result = solve<ForwardChecking, Heuristics>(puzzle);
	assert(result.solved);
	return {t.duration().millis(), result.searchedNodes};
}

int main(int argc, char*argv[])
{
	int reps = 1000;

	if (argc > 1) {
		reps = atoi(argv[1]);
		if (reps == 0) return 1;
	}

	Puzzle puzzle;

	// Read the values
	for (int i = 0; i < 81; ++i) {
		int value;
		cin >> value;
		if (value == 0) continue;
		puzzle[i] = value;
	}

	char const* tests[3]    { "B"   , "B+FC", "B+FC+H"};
	TimingResult results[3] { {0, 0}, {0, 0}, {0, 0} };

	for (int i = 0; i < reps; ++i) {
		results[0] += time_solve<false, false>(puzzle);
	}

	for (int i = 0; i < reps; ++i) {
		results[1] += time_solve<true, false>(puzzle);
	}

	for (int i = 0; i < reps; ++i) {
		results[2] += time_solve<true, true>(puzzle);
	}

	for (int i = 0; i < 3; ++i) {
		cout << tests[i] << ":\t"
		     << results[i].millis << "ms\t"
		     << results[i].nodes << "nodes" << endl;
	}

	return 0;
}
