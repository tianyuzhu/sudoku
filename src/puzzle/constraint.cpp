#include "constraint.h"

const Constraint Constraint::All((1 << Value::Max) - 1);
const Constraint Constraint::None(0);

Constraint::Constraint() :
allowed(0)
{}

Constraint::Constraint(field f) :
allowed(f)
{}

bool Constraint::add(Value value)
{
	if (value == 0) return true;
	field v = 1 << (value - 1);
	return allowed != (allowed |= v);
}

bool Constraint::remove(Value value)
{
	if (value == 0) return true;
	field v = 1 << (value - 1);
	return allowed != (allowed &= ~v);
}

bool Constraint::operator ==(Constraint c) const
{
	return allowed == c.allowed;
}

Constraint Constraint::operator &(Constraint c) const
{
	return allowed & c.allowed;
}

Constraint Constraint::operator |(Constraint c) const
{
	return allowed | c.allowed;
}

Constraint Constraint::operator ^(Constraint c) const
{
	return allowed ^ c.allowed;
}

Constraint &Constraint::operator <<=(int n)
{
	allowed <<= n;
	return *this;
}

int Constraint::bitCount() const
{
	int count = 0;
	for (field bits = allowed; bits > 0; ++count)
		bits &= bits - 1;

	return count;
}
