#include "value.h"
#include <cassert>

const Value Value::Indicies[Max] = {0, 1, 2, 3, 4, 5, 6, 7, 8};
const Value Value::Values[Max]   = {1, 2, 3, 4, 5, 6, 7, 8, 9};

Value::Value() :
value(Min)
{}

Value::Value(Value const& value) :
value(value.value)
{}

Value::Value(int value) :
value(value)
{
	assert(Min <= value && value <= Max);
}

Value::operator int() const
{
	return (int) value;
}

Value &Value::operator =(int value)
{
	assert(Min <= value && value <= Max);
	this->value = value;
	return *this;
}
