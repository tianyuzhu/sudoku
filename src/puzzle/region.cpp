#include "region.h"

static inline
Value block_row(Value index)
{
	return index / 3;
}

static inline
Value block_col(Value index)
{
	return index % 3;
}

namespace _ {

template<>
int index_of<ROW>(Value index)
{
	return 9*index;
}

template<>
int index_of<COLUMN>(Value index)
{
	return index;
}

template<>
int index_of<BLOCK>(Value index)
{
	return 27*block_row(index) + 3*block_col(index);
}

template<>
int index_in<ROW>(Value index)
{
	return index;
}

template<>
int index_in<COLUMN>(Value index)
{
	return 9*index;
}

template<>
int index_in<BLOCK>(Value index)
{
	return 9*block_row(index) + block_col(index);
}

}; // namespace _
