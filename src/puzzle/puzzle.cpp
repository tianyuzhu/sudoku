#include <cassert>
#include <cstddef>

#include "puzzle.h"

namespace _ {

/// Row Regions ///

template Region<Value, ROW>::Region(Value *, Value);
template Value &Region<Value, ROW>::operator [](Value);

// Column Regions ///

template Region<Value, COLUMN>::Region(Value *, Value);
template Value &Region<Value, COLUMN>::operator [](Value);

/// Block Regions ///

template Region<Value, BLOCK>::Region(Value *, Value);
template Value &Region<Value, BLOCK>::operator [](Value);

} // namespace _

/// Puzzle ///

Puzzle::Row Puzzle::row(Value index)
{
	return Row(entries, index);
}

Puzzle::Column Puzzle::column(Value index)
{
	return Column(entries, index);
}

Puzzle::Block Puzzle::block(Value index)
{
	return Block(entries, index);
}

Value &Puzzle::operator [](int index)
{
	assert(0 <= index && index < 81);
	return entries[index];
}

Puzzle::Coordinates Puzzle::coordsOf(Value &value)
{
	ptrdiff_t diff = &value - entries;
	assert (0 <= diff && diff < 81);
	Value row = diff / 9, col = diff % 9;
	Value blk_row = row / 3, blk_col = col / 3;
	return {row, col, 3*blk_row + blk_col};
}
