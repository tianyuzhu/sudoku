#include "puzzle.h"
#include "search.h"
#include <iostream>
#include <cstdlib>

using namespace std;

void printPuzzle(Puzzle &puzzle);

int main(int argc, char*argv[])
{
	int reps = 1000;

	if (argc > 1) {
		reps = atoi(argv[1]);
		if (reps == 0) return 1;
	}

	Puzzle puzzle;

	// Read the values
	for (int i = 0; i < 81; ++i) {
		int value;
		cin >> value;
		if (value == 0) continue;
		puzzle[i] = value;
	}

	Result result = solve<true,true>(puzzle);
	printPuzzle(puzzle);
	return result.solved? 0: 1;
}

void printPuzzle(Puzzle &puzzle)
{
	for (auto r : Value::Indicies) {
		auto row = puzzle.row(r);
		for (auto c : Value::Indicies) {
			cout << row[c] << ' ';
			if (c % 3 == 2) cout << "  ";
		}
		cout << '\n';
		if (r % 3 == 2) cout << '\n';
	}
	cout.flush();
}
