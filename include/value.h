#ifndef VALUE_H
#define VALUE_H

class Value
{
public:
	Value(); // Default constructible
	Value(Value const& value); // Copyable

	Value(int value); // Convertible from int
	operator int() const; // Convertible to int

	Value &operator =(int value); // Assignable from int

	enum {Min = 0, Max = 9};
	static const Value Indicies[Max];
	static const Value Values[Max];

private:
	char value;
};

#endif // VALUE_H
