#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include "value.h"
#include <cstdint>

class Constraint
{
	typedef std::uint16_t field;
public:
	Constraint();
	bool add(Value value);
	bool remove(Value value);

	bool operator ==(Constraint c) const;
	Constraint operator &(Constraint c) const;
	Constraint operator |(Constraint c) const;
	Constraint operator ^(Constraint c) const;

	Constraint &operator <<=(int n);

	int bitCount() const;

	static const Constraint All, None;
private:
	Constraint(field);
	field allowed;
};

#endif // CONSTRAINT_H
