#ifndef SEARCH_H
#define SEARCH_H

#include <iosfwd>

class Puzzle;

class Result
{
	friend std::ostream &operator <<(std::ostream &, const Result &);

public:
	Result();
	Result &operator +=(Result result);
	unsigned long searchedNodes;
	bool solved;
};

template<bool ForwardChecking, bool Heuristics>
Result solve(Puzzle &puzzle);

#endif // SEARCH_H
