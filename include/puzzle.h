#ifndef PUZZLE_H
#define PUZZLE_H

#include "value.h"
#include "region.h"

class Puzzle
{
	typedef Value Entries[81];
public:

	typedef _::Region<Value, _::ROW> Row;
	typedef _::Region<Value, _::COLUMN> Column;
	typedef _::Region<Value, _::BLOCK> Block;

	Row row(Value index);
	Column column(Value index);
	Block block(Value index);
	Value &operator [](int index);

	struct Coordinates {
		Value row;
		Value column;
		Value block;
	};

	Coordinates coordsOf(Value &value);

private:
	Entries entries;
};

#endif // PUZZLE_H
