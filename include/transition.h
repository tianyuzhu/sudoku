#ifndef TRANSITION_H
#define TRANSITION_H

#include <array>
#include <vector>

class Value;

class Puzzle;
class Assignment;

template<bool Heuristics>
class Transition;

template<>
class Transition<false>
{
public:
	Transition(Puzzle &, Assignment const&);
	Transition(Transition const&tr);

	Value *nextEntry();
	const Value (&valuesOf(Value *entry))[9];

private:
	Puzzle &puzzle;
	int index;
};

template<>
class Transition<true>
{
public:
	Transition(Puzzle &, Assignment const&);

	Value *nextEntry();
	std::vector<Value> valuesOf(Value *entry);

private:
	Puzzle &puzzle;
	Assignment const&asst;
};

#endif // TRANSITION_H
