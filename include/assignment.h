#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include "constraint.h"
#include "value.h"

class Assignment {
public:
	Constraint &row(Value index);
	Constraint const&row(Value index) const;

	Constraint &column(Value index);
	Constraint const&column(Value index) const;

	Constraint &block(Value index);
	Constraint const&block(Value index) const;

private:
	Constraint asst[3][Value::Max];
};

#endif // ASSIGNMENT_H
