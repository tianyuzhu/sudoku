#ifndef STATES_H
#define STATES_H

class Value;
class Puzzle;
class Assignment;

template<bool FC>
class Start;

template<bool FC>
class Assign
{
	friend class Start<FC>;

public:
	Assign(Assign const&) = delete;
	Assign(Assign &) = delete;
	Assign(Assign &&assign);
	~Assign();

	void cancelUndo();
	bool isValid();
	Assign<FC> assign(Value &entry, Value value);

private:
	Assign(
		Value &entry, Value value,
		Puzzle &puzzle,
		Assignment &asst);

	bool valid;
	Value *entry;
	Puzzle &puzzle;
	Assignment &asst;
};

template<bool FC>
class Start
{
public:
	Start(Puzzle &puzzle, Assignment &asst);

	void cancelUndo();
	bool isValid();
	Assign<FC> assign(Value &entry, Value value);

private:
	Puzzle &puzzle;
	Assignment &asst;
	bool valid;
};

#endif // STATES_H
