#ifndef REGION_H
#define REGION_H

#include "value.h"

namespace _ {

enum RegionType {ROW, COLUMN, BLOCK};

template<typename T, RegionType R>
class Region
{
public:
	Region(T *entries, Value index);

	T &operator [](Value index);

private:
	T *region;
};

template<RegionType R>
int index_of(Value index);

template<typename T, RegionType R>
Region<T, R>::Region(T *entries, Value index) :
region(entries + index_of<R>(index))
{}

template<RegionType R>
int index_in(Value index);

template<typename T, RegionType R>
T &Region<T, R>::operator [](Value index)
{
	return region[index_in<R>(index)];
}

} // namespace _

#endif // REGION_H
