#include "constraint.h"
#include "gtest/gtest.h"

typedef uint16_t field;

field &value_of(Constraint &c)
{
	return *reinterpret_cast<field *>(&c);
}

field value_of(Constraint &&c)
{
	return *reinterpret_cast<field *>(&c);
}

TEST(Constraint, Construct)
{
	Constraint c;
	EXPECT_EQ(0, value_of(c));
}

TEST(Constraint, Equals)
{
	Constraint c1, c2;
	c1.add(1);
	c2.add(1);

	EXPECT_TRUE(c1 == c2);
	EXPECT_TRUE(c2 == c1);
}

TEST(Constraint, EqualsNot)
{
	Constraint c1, c2;
	c1.add(1);
	c2.add(5);

	EXPECT_FALSE(c1 == c2);
	EXPECT_FALSE(c2 == c1);
}

TEST(Constraint, AndSame)
{
	Constraint c1, c2;
	c1.add(2);
	c2.add(2);

	EXPECT_EQ(2, value_of(c1 & c2));
}

TEST(Constraint, AndDiff)
{
	Constraint c1, c2;
	c1.add(3);
	c1.add(2);
	c2.add(2);
	c2.add(1);

	EXPECT_EQ(2, value_of(c1 & c2));
}

TEST(Constraint, OrSame)
{
	Constraint c1, c2;
	c1.add(2);
	c2.add(2);

	EXPECT_EQ(2, value_of(c1 | c2));
}

TEST(Constraint, OrDiff)
{
	Constraint c1, c2;
	c1.add(3);
	c1.add(2);
	c2.add(2);
	c2.add(1);

	EXPECT_EQ(7, value_of(c1 | c2));
}

TEST(Constraint, XorSame)
{
	Constraint c1, c2;
	c1.add(2);
	c2.add(2);

	EXPECT_EQ(0, value_of(c1 ^ c2));
}

TEST(Constraint, XorDiff)
{
	Constraint c1, c2;
	c1.add(3);
	c1.add(2);
	c2.add(2);
	c2.add(1);

	EXPECT_EQ(5, value_of(c1 ^ c2));
}

TEST(Constraint, Add0)
{
	Constraint c;
	EXPECT_TRUE(c.add(0));
	EXPECT_TRUE(c.add(0));
}

TEST(Constraint, Remove0)
{
	Constraint c;
	EXPECT_TRUE(c.remove(0));
	EXPECT_TRUE(c.remove(0));
}

TEST(Constraint, ShiftLeftEq)
{
	Constraint c;
	EXPECT_EQ(0, value_of(c));

	c <<= 1;
	EXPECT_EQ(0, value_of(c));

	EXPECT_TRUE(c.add(3));
	c <<= 1;
	EXPECT_TRUE(c.remove(4));
}

TEST(Constraint, BitCount0)
{
	Constraint c;
	EXPECT_EQ(0, c.bitCount());
}

TEST(Constraint, BitCount1)
{
	Constraint c;
	c.add(6);
	EXPECT_EQ(1, c.bitCount());
}

TEST(Constraint, BitCount3)
{
	Constraint c;
	c.add(5);
	c.add(2);
	c.add(4);
	EXPECT_EQ(3, c.bitCount());
}

class ValueOps : public ::testing::TestWithParam<Value>
{};

TEST_P(ValueOps, Add)
{
	const Value p = GetParam();
	Constraint c;
	EXPECT_TRUE(c.add(p));
	EXPECT_EQ(1<<(p-1), value_of(c));
	EXPECT_FALSE(c.add(p));
	EXPECT_EQ(1<<(p-1), value_of(c));
	c.remove(p);
	EXPECT_EQ(0, value_of(c));
	EXPECT_TRUE(c.add(p));
	EXPECT_EQ(1<<(p-1), value_of(c));
}

TEST_P(ValueOps, Remove)
{
	const Value p = GetParam();
	Constraint c;
	EXPECT_FALSE(c.remove(p));
	EXPECT_EQ(0, value_of(c));
	c.add(p);
	EXPECT_EQ(1<<(p-1), value_of(c));
	EXPECT_TRUE(c.remove(p));
	EXPECT_EQ(0, value_of(c));
	EXPECT_FALSE(c.remove(p));
	EXPECT_EQ(0, value_of(c));
}

INSTANTIATE_TEST_CASE_P(Constraint, ValueOps,
	::testing::ValuesIn(Value::Values));
