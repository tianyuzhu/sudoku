#include "gtest/gtest.h"
#include "puzzle.h"

static char &value_of(Value &value)
{
	return *reinterpret_cast<char *>(&value);
}

static void expect_int(Value actual, int expected)
{
	EXPECT_EQ(value_of(actual), expected);
}

/// Positive tests ///

TEST(Value, DefaultConstructible)
{
	Value v;
	EXPECT_EQ(value_of(v), 0);
}

TEST(Value, CopyDefault)
{
	Value v;
	Value copy(v);
	EXPECT_EQ(value_of(copy), 0);
}

TEST(Value, Indicies)
{
	Value expected = Value::Min;
	for (auto index : Value::Indicies) {
		EXPECT_EQ(expected, index);
		expected = expected + 1;
	}
}

TEST(Value, Values)
{
	Value expected = Value::Min;
	for (auto value : Value::Values) {
		expected = expected + 1;
		EXPECT_EQ(expected, value);
	}
}

class Positive : public ::testing::TestWithParam<int>
{};

TEST_P(Positive, Copyable)
{
	Value v(GetParam());
	Value copy(v);
	EXPECT_EQ(value_of(copy), GetParam());
}

TEST_P(Positive, ConstructibleFromInt)
{
	Value v(GetParam());
	EXPECT_EQ(value_of(v), GetParam());
}

TEST_P(Positive, ConvertibleFromInt)
{
	expect_int(int(GetParam()), GetParam());
}

TEST_P(Positive, AssignableFromInt)
{
	Value v;
	EXPECT_EQ(value_of(v = GetParam()), GetParam());
}

TEST_P(Positive, ConvertibleToInt)
{
	Value v(GetParam());
	EXPECT_EQ(int(v), GetParam());
}

INSTANTIATE_TEST_CASE_P(Value, Positive, ::testing::Range(0, 10));

/// Negative tests ///

class Negative : public ::testing::TestWithParam<int>
{};

TEST_P(Negative, ConstructibleFromInt)
{
	::testing::FLAGS_gtest_death_test_style = "threadsafe";
	ASSERT_DEATH(Value v(GetParam()), ".*");
}

TEST_P(Negative, ConvertibleFromInt)
{
	::testing::FLAGS_gtest_death_test_style = "threadsafe";
	ASSERT_DEATH(expect_int(int(GetParam()), GetParam()), ".*");
}

TEST_P(Negative, AssignableFromInt)
{
	::testing::FLAGS_gtest_death_test_style = "threadsafe";
	Value v;
	ASSERT_DEATH(v = GetParam(), ".*");
}

#ifndef NDEBUG
INSTANTIATE_TEST_CASE_P(Value, Negative, ::testing::Values(-1, 10));
#endif //NDEBUG
