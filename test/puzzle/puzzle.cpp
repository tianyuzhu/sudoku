#include "gtest/gtest.h"

#include "puzzle.h"

template<typename T, _::RegionType R>
T *entries_of(_::Region<T, R> &region)
{
	return &region[0];
}

Value *entries_of(Puzzle &puzzle)
{
	return reinterpret_cast<Value *>(&puzzle);
}

class Entries : public ::testing::TestWithParam<int>
{};

TEST_P(Entries, Entry)
{
	const int p = GetParam();
	Puzzle puzzle;

	Value &value = puzzle[p];
	EXPECT_EQ(entries_of(puzzle) + p, &value);
}

INSTANTIATE_TEST_CASE_P(Puzzle, Entries, ::testing::Range(0,81,7));

class Coordinates : public ::testing::TestWithParam<int>
{};

static const int
test_index[]   = {0, 18, 1, 40, 74, 24},
expected_row[] = {0,  2, 0,  4,  8,  2},
expected_col[] = {0,  0, 1,  4,  2,  6},
expected_blk[] = {0,  0, 0,  4,  6,  2};

TEST_P(Coordinates, OfIndex)
{
	int i = GetParam();
	Puzzle puzzle;
	auto coords = puzzle.coordsOf(puzzle[test_index[i]]);
	EXPECT_EQ(expected_row[i], coords.row);
	EXPECT_EQ(expected_col[i], coords.column);
	EXPECT_EQ(expected_blk[i], coords.block);
}

INSTANTIATE_TEST_CASE_P(
	Puzzle, Coordinates,
	::testing::Range(0, 6));
