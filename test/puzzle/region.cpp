#include "region.h"
#include "gtest/gtest.h"

using namespace _;

typedef Region<int, ROW> Row;
typedef Region<int, COLUMN> Column;
typedef Region<int, BLOCK> Block;

static int entries[81] {
	0, 1, 2, 3, 4, 5, 6, 7, 8,
	1, 2, 3, 4, 5, 6, 7, 8, 0,
	2, 3, 4, 5, 6, 7, 8, 0, 1,
	3, 4, 5, 6, 7, 8, 0, 1, 2,
	4, 5, 6, 7, 8, 0, 1, 2, 3,
	5, 6, 7, 8, 0, 1, 2, 3, 4,
	6, 7, 8, 0, 1, 2, 3, 4, 5,
	7, 8, 0, 1, 2, 3, 4, 5, 6,
	8, 0, 1, 2, 3, 4, 5, 6, 7
};

static const int expected_rows[9][9] {
	{0, 1, 2, 3, 4, 5, 6, 7, 8},
	{1, 2, 3, 4, 5, 6, 7, 8, 0},
	{2, 3, 4, 5, 6, 7, 8, 0, 1},
	{3, 4, 5, 6, 7, 8, 0, 1, 2},
	{4, 5, 6, 7, 8, 0, 1, 2, 3},
	{5, 6, 7, 8, 0, 1, 2, 3, 4},
	{6, 7, 8, 0, 1, 2, 3, 4, 5},
	{7, 8, 0, 1, 2, 3, 4, 5, 6},
	{8, 0, 1, 2, 3, 4, 5, 6, 7}
};

static const int (&expected_columns)[9][9] = expected_rows;

static const int expected_blocks[9][9] {
	{0, 1, 2, 1, 2, 3, 2, 3, 4},
	{3, 4, 5, 4, 5, 6, 5, 6, 7},
	{6, 7, 8, 7, 8, 0, 8, 0, 1},
	{3, 4, 5, 4, 5, 6, 5, 6, 7},
	{6, 7, 8, 7, 8, 0, 8, 0, 1},
	{0, 1, 2, 1, 2, 3, 2, 3, 4},
	{6, 7, 8, 7, 8, 0, 8, 0, 1},
	{0, 1, 2, 1, 2, 3, 2, 3, 4},
	{3, 4, 5, 4, 5, 6, 5, 6, 7},
};

class Rows : public ::testing::TestWithParam<int>
{};

TEST_P(Rows, Index)
{
	const Value p = GetParam();
	const int (&expected)[9] = expected_rows[p];
	Row row(entries, p);

	for (int i = 0; i < 9; i++) {
		EXPECT_EQ(expected[i], row[i]);
	}
}

INSTANTIATE_TEST_CASE_P(, Rows, ::testing::Range(0,9));

class Columns : public ::testing::TestWithParam<int>
{};

TEST_P(Columns, Index)
{
	const Value p = GetParam();
	const int (&expected)[9] = expected_columns[p];
	Column col(entries, p);

	for (int i = 0; i < 9; i++) {
		EXPECT_EQ(expected[i], col[i]);
	}
}

INSTANTIATE_TEST_CASE_P(, Columns, ::testing::Range(0,9));

class Blocks : public ::testing::TestWithParam<int>
{};

TEST_P(Blocks, Index)
{
	const Value p = GetParam();
	const int (&expected)[9] = expected_blocks[p];
	Block blk(entries, p);

	for (int i = 0; i < 9; i++) {
		EXPECT_EQ(expected[i], blk[i]);
	}
}

INSTANTIATE_TEST_CASE_P(, Blocks, ::testing::Range(0,9));
