#include "gtest/gtest.h"
#include "puzzle.h"
#include "states.h"
#include "assignment.h"

/// Assign ///

TEST(Assign, Undo)
{
	Puzzle puzzle;
	Assignment asst;
	Start<false> start(puzzle, asst);
	Value &entry = puzzle[0];
	EXPECT_EQ(0, entry);

	{
		Assign<false> assign = start.assign(entry, 1);
		ASSERT_EQ(1, entry);
	}

	ASSERT_EQ(0, entry);
}

TEST(Assign, CancelUndo)
{
	Puzzle puzzle;
	Assignment asst;
	Start<false> start(puzzle, asst);
	Value &entry = puzzle[0];
	EXPECT_EQ(0, entry);

	{
		Assign<false> assign = start.assign(entry, 1);
		ASSERT_EQ(1, entry);
		assign.cancelUndo();
	}

	ASSERT_EQ(1, entry);
}

TEST(Assign, Valid)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;

	Start<false> start(puzzle, asst);
	Assign<false> assign = start.assign(puzzle[1], 2);
	EXPECT_TRUE(assign.isValid());
}

TEST(Assign, InvalidRow)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;

	Start<false> start(puzzle, asst);
	Assign<false> assign = start.assign(puzzle[4], 1);
	EXPECT_FALSE(assign.isValid());
}

TEST(Assign, InvalidColumn)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;

	Start<false> start(puzzle, asst);
	Assign<false> assign = start.assign(puzzle[18], 1);
	EXPECT_FALSE(assign.isValid());
}

TEST(Assign, InvalidBlock)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;

	Start<false> start(puzzle, asst);
	Assign<false> assign = start.assign(puzzle[10], 1);
	EXPECT_FALSE(assign.isValid());
}

TEST(Assign, Assign)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;

	Start<false> start(puzzle, asst);
	Assign<false> assign1 = start.assign(puzzle[1], 2);
	Assign<false> assign2 = assign1.assign(puzzle[9], 3);
}

/// Start ///

TEST(Start, Undo)
{
	Puzzle puzzle;
	Assignment asst;

	{
		Start<false> start(puzzle, asst);
		(void) start;
	}
}

TEST(Start, CancelUndo)
{
	Puzzle puzzle;
	Assignment asst;
	{
		Start<false> start(puzzle, asst);
		start.cancelUndo();
	}
}

TEST(Start, Valid)
{
	Puzzle puzzle;
	Assignment asst;
	Start<false> start(puzzle, asst);
	EXPECT_TRUE(start.isValid());
}

TEST(Start, InvalidRow)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[0] = 1;
	puzzle[1] = 1;

	Start<false> start(puzzle, asst);
	EXPECT_FALSE(start.isValid());
}

TEST(Start, InvalidColumn)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[30] = 1;
	puzzle[75] = 1;

	Start<false> start(puzzle, asst);
	EXPECT_FALSE(start.isValid());
}

TEST(Start, InvalidBlock)
{
	Puzzle puzzle;
	Assignment asst;
	puzzle[ 6] = 1;
	puzzle[26] = 1;

	Start<false> start(puzzle, asst);
	EXPECT_FALSE(start.isValid());
}

TEST(Start, Assign)
{
	Puzzle puzzle;
	Assignment asst;
	Start<false> start(puzzle, asst);
	Assign<false> assign = start.assign(puzzle[5], 3);
}
